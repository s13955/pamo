package pja.pamo.mediplanner.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Database access helper class
 */
public class Database {

    public static AppDatabase get(Context context) {
        return Room
                .databaseBuilder(context, AppDatabase.class, "mediplanner.db")
                .addMigrations(new Migration(1, 2) {
                    @Override
                    public void migrate(@NonNull SupportSQLiteDatabase database) {

                    }
                })
                .allowMainThreadQueries()
                .build();
    }

}
