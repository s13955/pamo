package pja.pamo.mediplanner.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import pja.pamo.mediplanner.database.dao.DrugDAO;
import pja.pamo.mediplanner.database.entity.Drug;

/**
 * Database manager using RoomAPI library which
 */
@Database(entities = {Drug.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    /**
     * Gets drug DAO (Data Access Object)
     * @return DrugDAO
     */
    public abstract DrugDAO getDrugDAO();
}
