package pja.pamo.mediplanner.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import pja.pamo.mediplanner.database.entity.Drug;

/**
 * Drug DAO using RoomAPI
 */
@Dao
public interface DrugDAO {

    /**
     * Inserts drug(s) into database
     *
     * @param drugs
     */
    @Insert
    void insert(Drug... drugs);

    /**
     * Updates drug(s) in database
     *
     * @param drugs
     */
    @Update
    void update(Drug... drugs);

    /**
     * Deletes drug in database
     *
     * @param drug
     */
    @Delete
    void delete(Drug drug);

    /**
     * Return all drugs order alphabetically
     *
     * @return Drug[]
     */
    @Query("SELECT * FROM drug ORDER BY name ASC")
    List<Drug> getDrugs();

    /**
     * Returns drug with specified id
     *
     * @param id
     * @return Drug
     */
    @Query("SELECT * FROM drug WHERE id = :id")
    Drug getDrugById(String id);
}
