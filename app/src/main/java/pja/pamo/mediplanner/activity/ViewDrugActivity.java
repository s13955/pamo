package pja.pamo.mediplanner.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import pja.pamo.mediplanner.R;
import pja.pamo.mediplanner.database.Database;
import pja.pamo.mediplanner.database.dao.DrugDAO;
import pja.pamo.mediplanner.database.entity.Drug;

/**
 * ViewDrugActivity is responsible for viewing persisted drug details
 */
public class ViewDrugActivity extends BaseActivity {

    private TextView id;
    private TextView name;
    private TextView description;

    private DrugDAO drugDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_drug);

        // Back arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // DAO
        drugDAO = Database.get(this).getDrugDAO();

        // ID
        String drugId = getIntent().getStringExtra("drug_id");
        Drug drug = drugDAO.getDrugById(drugId);

        // Fields
        id = findViewById(R.id.text_id);
        name = findViewById(R.id.text_name);
        description = findViewById(R.id.text_description);

        id.setText(drugId);
        name.setText(drug.getName());
        description.setText(drug.getDescription());


        Button delete = findViewById(R.id.button_delete);
        delete.setOnClickListener((view) -> {
            drugDAO.delete(drug);
            gotoMainActivity();
        });

        Button edit = findViewById(R.id.button_edit);
        edit.setOnClickListener((view) -> {
            Intent intent = new Intent(ViewDrugActivity.this, AddDrugActivity.class);
            intent.putExtra("drug_id", drugId);
            startActivity(intent);
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
