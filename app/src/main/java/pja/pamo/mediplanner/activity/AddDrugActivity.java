package pja.pamo.mediplanner.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.UUID;

import pja.pamo.mediplanner.R;
import pja.pamo.mediplanner.database.Database;
import pja.pamo.mediplanner.database.dao.DrugDAO;
import pja.pamo.mediplanner.database.entity.Drug;

/**
 * Add/Edit drug activity
 */
public class AddDrugActivity extends BaseActivity {

    private EditText name;
    private EditText description;

    private DrugDAO drugDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_drug);

        // Back arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Fields
        Button save = findViewById(R.id.button_add);
        name = findViewById(R.id.text_id);
        description = findViewById(R.id.text_description);

        // DrugID
        String drugId = getIntent().getStringExtra("drug_id");

        // Database
        drugDAO = Database.get(this).getDrugDAO();

        Drug drug = null;
        if (drugId != null) {
            drug = drugDAO.getDrugById(drugId);
            name.setText(drug.getName());
            description.setText(drug.getDescription());
            save.setText("Aktualizuj");
        }

        Drug finalDrug = drug;

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drugId == null) {
                    Drug drug = Drug.builder()
                            .id(String.valueOf(UUID.randomUUID()))
                            .name(name.getText().toString())
                            .description(description.getText().toString())
                            .build();

                    drugDAO.insert(drug);
                } else {
                    finalDrug.setName(name.getText().toString());
                    finalDrug.setDescription(description.getText().toString());
                    drugDAO.update(finalDrug);
                }
                gotoMainActivity();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        gotoMainActivity();
        return true;
    }
}
