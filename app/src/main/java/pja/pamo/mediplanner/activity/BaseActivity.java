package pja.pamo.mediplanner.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

/**
 * BaseActivity providing common functionality across activities
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected void gotoMainActivity() {
        Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(mainActivity);
    }

}
