package pja.pamo.mediplanner.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import pja.pamo.mediplanner.R;
import pja.pamo.mediplanner.database.Database;
import pja.pamo.mediplanner.database.dao.DrugDAO;
import pja.pamo.mediplanner.database.entity.Drug;
import pja.pamo.mediplanner.recycler.RecyclerViewAdapter;

/**
 * Main activity responsible for listing persisted drugs
 */
public class MainActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private RecyclerViewAdapter adapter;

    private DrugDAO drugDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Database
        drugDAO = Database.get(this).getDrugDAO();

        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // FAB
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addDrugIntent = new Intent(MainActivity.this, AddDrugActivity.class);
                startActivity(addDrugIntent);
            }
        });

        adapter = new RecyclerViewAdapter(this, new ArrayList<Drug>(), new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Drug item) {
                Intent details = new Intent(MainActivity.this, ViewDrugActivity.class);
                details.putExtra("drug_id", item.getId());
                startActivity(details);
            }
        });

        // RecyclerView
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        adapter.updateData(drugDAO.getDrugs());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
