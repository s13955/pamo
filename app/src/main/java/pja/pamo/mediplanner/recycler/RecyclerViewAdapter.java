package pja.pamo.mediplanner.recycler;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pja.pamo.mediplanner.R;
import pja.pamo.mediplanner.database.entity.Drug;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<Drug> drugs;
    private OnItemClickListener clickListener;

    public RecyclerViewAdapter(Context context, List<Drug> drugs, OnItemClickListener clickListener) {
        this.context = context;
        this.drugs = drugs;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycler_contact, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder viewHolder, int i) {
        viewHolder.bindData(i, clickListener);
    }

    @Override
    public int getItemCount() {
        return drugs.size();
    }

    public void updateData(List<Drug> drugs) {
        this.drugs = drugs;
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onItemClick(Drug item);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private TextView initialsTextView;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.nameTextView);
            initialsTextView = itemView.findViewById(R.id.initialsTextView);
        }

        void bindData(int position, final OnItemClickListener clickListener) {
            final Drug drug = drugs.get(position);

            String name = drug.getName();
            nameTextView.setText(name);

            String initial = drug.getName().toUpperCase().substring(0, 1);
            initialsTextView.setText(initial);
            GradientDrawable background = (GradientDrawable) initialsTextView.getBackground();
            background.setColor(ContextCompat.getColor(context, R.color.colorPrimary));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onItemClick(drug);
                }
            });
        }
    }
}
